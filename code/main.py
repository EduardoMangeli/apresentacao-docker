from fastapi import FastAPI
import socket

app = FastAPI()

@app.get("/")
def read_root():
    s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
    s.connect(("8.8.8.8", 80))
    ip = s.getsockname()[0]
    s.close()
    return {"data": "oi", "ip": ip}
